import React from 'react';
import Banner from '../components/Banner';
import Head from 'next/head';



export default function Home() {

    const data = {
        title: "More More Money using Money Monitor!",
        content: "Keep an eye on your budget!",
        destination:"/",
    }

    return (
        <React.Fragment>
            <Head>
                <title>Home</title>
            </Head>
            <Banner data={data}/>
        </React.Fragment>
    )
}